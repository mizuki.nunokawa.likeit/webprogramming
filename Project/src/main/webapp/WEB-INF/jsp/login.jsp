<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="ja">

<head>
    <head>
        <meta charset="utf-8">
        <title>ログイン画面</title>
        <link href="css/login.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
        crossorigin="anonymous">

       
      </head>
</head>

<body>
    <h1 class="logintitle">ログイン画面</h1>

    <div>
    <!-- formタグ内の記載でデータを送るサーブレットをメソッドを指定 -->
        <form class="form-login" action="LoginServlet" method="post">
            <div class="row">
              <div class="text col-4">
                <label for="">ログインID</label>
              </div>
              <div class="text col-6">
                <input type="text" name="loginId" id="inputLoginId" class="form-control"  placeholder="ログインID" required autofocus>
              </div>
            </div>
            <br>

            <div class="row">
                <div class="text col-4">
                  <label for="">パスワード</label>
                </div>
                <div class="text col-6">
                  <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required>
                </div>
              </div>
              <br>
              <div class="text">
                <button type="submit" class="btn btn-secondary btn-lg">ログイン</button>
              </div>



          </form>
    </div>
        
    


</body>



</html>
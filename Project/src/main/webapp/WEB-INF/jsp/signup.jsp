<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="ja">

<head>
    <head>
        <meta charset="utf-8">
        <title>ログイン画面</title>
        <link href="css/signup.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
        crossorigin="anonymous">

       
      </head>
</head>

<body>
    <header>
        <ul>
            <li><a class="username" href="">ユーザー名さん</a></li>
            <li><a class="logout" href="">ログアウト</a></li>
        </ul>
    </header>

    <h1 class="logintitle">ユーザー新規登録</h1>

    <div>
        <form class="form-signup" action="SignUpServlet" method="post">
            <div class="row">
              <div class="text col-4">
                <label for="">ログインID</label>
              </div>
              <div class="text col-6">
                <input type="text" name="loginId" class="form-control" >
              </div>
            </div>
            <br>

            <div class="row">
                <div class="text col-4">
                  <label for="">パスワード</label>
                </div>
                <div class="text col-6">
                  <input type="password" name="password" class="form-control" >
                </div>
              </div>
              <br>
             
              <div class="row">
                <div class="text col-4">
                  <label for="">パスワード（確認）</label>
                </div>
                <div class="text col-6">
                  <input type="password" name="pass" class="form-control" >
                </div>
              </div>
              <br>

              <div class="row">
                <div class="text col-4">
                  <label for="">ユーザー名</label>
                </div>
                <div class="text col-6">
                  <input type="text" name="userName" class="form-control" >
                </div>
              </div>
              <br>

              <div class="row">
                <div class="text col-4">
                  <label for="">生年月日</label>
                </div>
                <div class="text col-6">
                  <input type="text" name="birthDay" class="form-control" >
                </div>
              </div>
              <br>

              <div class="text">
              	
                <button type="submit" class="btn btn-secondary btn-lg">登録</button>
              </div>

          </form>

          <div class="return">
            <a href="">戻る</a>
          </div>
    </div>
        
    


</body>



</html>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>ユーザー一覧</title>
        <link href="css/userlist.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
        crossorigin="anonymous">

    </head>
    <body>
        
        <header>
            <ul>
                <li><a class="username" href="">ユーザー名さん</a></li>
                <li><a class="logout" href="">ログアウト</a></li>
            </ul>
        </header>

        <h1 class="userlisttitle">ユーザ一覧</h1>

        <div class="signup">
       
          <a href="SignUpServlet">新規登録</a>
          
          
        </div>


        <div>
            <form>
                <div class="row">
                  <div class="text col-4">
                    <label for="">ログインID</label>
                  </div>
                  <div class="text col-6">
                    <input type="text" class="form-control" >
                  </div>
                </div>
    
                <div class="row">
                    <div class="text col-4">
                      <label for="">パスワード</label>
                    </div>
                    <div class="text col-6">
                      <input type="text" class="form-control" >
                    </div>
                  </div>

                  <div class="row">
                    <div class="text col-4">
                      <label for="">生年月日</label>
                    </div>
                    <div class="col-3">
                        <input type="text" class="form-control" placeholder="年/月/日">
                      </div>
                      
                      <div class="col-3">
                        <input type="text" class="form-control" placeholder="年/月/日">
                      </div>
                  </div>

                  <br>
                  <div class="textbtn">
                    <button type="button" class="btn btn-secondary btn-lg">検索</button>
                  </div>
    
              </form>
        </div>
        <br>
        <br>
        <br>

        <hr>

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">ログインID</th>
                <th scope="col">ユーザー名</th>
                <th scope="col">生年月日</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
                 <c:forEach var="user" items="${userlist}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <td>
                     <div class="btn-group" role="group" aria-label="Basic example">
		                    <button type="button" class="btn btn-secondary" href="UserDetailServlet?id=${user.id}">詳細</button>
		                    <button type="button" class="btn btn-secondary" href="UserDetailServlet?id=${user.id}">更新</button>
		                    <button type="button" class="btn btn-secondary" href="UserDetailServlet?id=${user.id}">削除</button>
		             </div> 
                     </td>
                   </tr>
                 </c:forEach>
            </tbody>
          </table>


    </body>
</html>
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Userbeans.User;

public class UserDao {

	public User findByloginInfo(String loginId,String password) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理		・・・②
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public List<User> findAll(){
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}
	
	//戻り値をつけないとサーブレットにおいて失敗時の文(if(user==null))をかけないが、
	//戻り値がnullじゃない場合何を返せば良いのかわからない。戻り値なしであれば失敗の処理はできないがうまくいく
	public void signnupinfo(String loginId,String password,String userName,String birthDay) {
		
		Connection conn = null;
		
		try {
			//コネクション
			conn = DBManager.getConnection();
			
			// SELECT文を準備。""の中はsquel proにて記入しコピーして持ってくること
	        String sql = "INSERT into user (login_id,name,birth_date,PASSWORD)values(?,?,?,?)";
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1,loginId);
	        pStmt.setString(2,userName);
	        pStmt.setString(3,birthDay);
	        pStmt.setString(4,password);

	        pStmt.executeUpdate();
	        

		 } catch (SQLException e) {
	         e.printStackTrace();
	     } finally {
	         // データベース切断
	         if (conn != null) {
	             try {
	                 conn.close();
	             } catch (SQLException e) {
	                 e.printStackTrace();
	             }
	         }	
	        
		}
		
	
	}
	
	public user detailReference() {
		
		Connection conn = null;
		
		try {
	}
		
	 } catch (SQLException e) {
         e.printStackTrace();
     } finally {
         // データベース切断
         if (conn != null) {
             try {
                 conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
             }
         }	
        
	}

}
